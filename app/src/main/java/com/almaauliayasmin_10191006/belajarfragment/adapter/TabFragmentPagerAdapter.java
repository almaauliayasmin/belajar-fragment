package com.almaauliayasmin_10191006.belajarfragment.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.almaauliayasmin_10191006.belajarfragment.fragment.Tab1Fragment;
import com.almaauliayasmin_10191006.belajarfragment.fragment.Tab2Fragment;

public abstract class TabFragmentPagerAdapter<position> extends FragmentPagerAdapter {
    //nama tabnya
    String[] title = {
         "Tab 1", "Tab 2"
    };

    public TabFragmentPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    //method ini yang akan memanipulasi penampilan Fragment  di layar

    public Fragment getItem(int position) {
      Fragment fragment = null;
      switch (position) {
        case 0:
          fragment = new Tab1Fragment();
          break;
        case 1:
          fragment = new Tab2Fragment();
          break;
        default:
          fragment = null;
          break;
        }
        return fragment;
    }

    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    public int getCount() {
        return title.length;
    }
}