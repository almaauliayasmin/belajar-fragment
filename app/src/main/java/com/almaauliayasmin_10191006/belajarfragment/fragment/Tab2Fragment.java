package com.almaauliayasmin_10191006.belajarfragment.fragment;

import androidx.fragment.app.Fragment;

import com.almaauliayasmin_10191006.belajarfragment.R;
import com.google.android.filament.View;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab2Fragment extends Fragment {

    public Tab2Fragment() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab1, container, false);
    }
}